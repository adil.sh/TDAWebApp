
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/InsertUpdateDeleteMembre"})
public class InsertUpdateDeleteMembre extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet InsertUpdateDeleteMembre</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet InsertUpdateDeleteMembre at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }
  
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
            PrintWriter out= response.getWriter();
        try {
            // Employe
                String prenomEmploye = request.getParameter("prenomEmploye");
                String nomEmploye = request.getParameter("nomEmploye");
                String usernameEmploye = request.getParameter("usernameEmploye");
                String emailEmploye = request.getParameter("emailEmploye");    
                String passwordEmploye = request.getParameter("passwordEmploye");
             // Employe Modifier
                 String idemploye  = request.getParameter("idemploye");
                String prenomEmployeModifier = request.getParameter("prenomEmployeModifier");
                String nomEmployeModifier = request.getParameter("nomEmployeModifier");
                String usernameEmployeModifier = request.getParameter("usernameEmployeModifier");
                String emailEmployeModifier = request.getParameter("emailEmployeModifier");    
                String passwordEmployeModifier = request.getParameter("passwordEmployeModifier");
                
                String idemployeSupprimer = request.getParameter("idemployeSupprimer");
                
                
            // Membre 
                String prenom = request.getParameter("prenom");
                String nom = request.getParameter("nom");
                String username = request.getParameter("username");
                String email = request.getParameter("email");    
                String password = request.getParameter("password");
            // Membre Modifier
                String idmembre  = request.getParameter("idMembre");
                String idmembreSupprimer  = request.getParameter("idMembreSupprimer");
                String prenomModifier = request.getParameter("prenomModifier");
                String nomModifier = request.getParameter("nomModifier");
                String usernameModifier = request.getParameter("usernameModifier");
                String emailModifier = request.getParameter("emailModifier");    
                String passwordModifier = request.getParameter("passwordModifier");
                
                // Connection
                Class.forName("com.mysql.jdbc.Driver");
                java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/tdadb","root",""); 
                
                /*   inserting membre the data from database*/
                Statement st = con.createStatement();
                ResultSet rs;
                if(username!=null){
                int i=st.executeUpdate("insert into membre(nom,prenom,username,email,password) values ('"+nom+"','"+prenom+"','"+username+"','"+email+"','"+password+"')"); 
                if (i > 0) {
                    response.sendRedirect("indexAdmin.jsp#membres");
                     out.println("<center><h2>inserted Succesfully</h2></center>");
                }
                }
                
                /*   inserting Employe the data from database*/
                Statement stE = con.createStatement();
                ResultSet rsE;
                if(usernameEmploye!=null){
                int iE=stE.executeUpdate("insert into employe(nom,prenom,username,email,password) values ('"+nomEmploye+"','"+prenomEmploye+"','"+usernameEmploye+"','"+emailEmploye+"','"+passwordEmploye+"')"); 
                if (iE > 0) {
                    response.sendRedirect("indexAdmin.jsp");
                     out.println("<center><h2>inserted Succesfully</h2></center>");
                }
                }
                
                
                /*   updating Membre data from database*/
               PreparedStatement stmt=con.prepareStatement("Update membre set nom=?,prenom=?,username=?,email=?,password=? where id="+idmembre);

               stmt.setString(1,nomModifier);
               stmt.setString(2,prenomModifier);
               stmt.setString(3,usernameModifier);
               stmt.setString(4,emailModifier);
               stmt.setString(5,passwordModifier);

               int e=stmt.executeUpdate();
               if (e > 0) {
                    response.setHeader("refresh","0;indexAdmin.jsp#membres");
                    out.println("<center><h2>updated Succesfully</h2></center>");
               } 
               
               // Updating emplotye from DB
                 PreparedStatement stmtE=con.prepareStatement("Update employe set nom=?,prenom=?,username=?,email=?,password=? where id="+idemploye);

               stmt.setString(1,nomEmployeModifier);
               stmt.setString(2,prenomEmployeModifier);
               stmt.setString(3,usernameEmployeModifier);
               stmt.setString(4,emailEmployeModifier);
               stmt.setString(5,passwordEmployeModifier);

               int eM=stmtE.executeUpdate();
               if (eM > 0) {
                    response.setHeader("refresh","0;indexAdmin.jsp");
                    out.println("<center><h2>updated Succesfully</h2></center>");
               } 
                /*deleting Employe data from database*/
                PreparedStatement stmt4=con.prepareStatement("delete from employe where id="+idemployeSupprimer);
                int dE=stmt4.executeUpdate();
                if (dE > 0) {
                        response.setHeader("refresh","0;indexAdmin.jsp");
                        out.println("<center><h2>Deleted Successfully</h2></center>");
                } 

                /*deleting the data from database*/
                PreparedStatement stmt3=con.prepareStatement("delete from membre where id="+idmembreSupprimer);
                int d=stmt3.executeUpdate();
                if (d > 0) {
                        response.setHeader("refresh","0;indexAdmin.jsp#membres");
                        out.println("<center><h2>Deleted Successfully</h2></center>");
                } 

                    } catch (ClassNotFoundException | SQLException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                    }         
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

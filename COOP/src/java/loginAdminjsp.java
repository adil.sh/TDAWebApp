import LoginDao.LoginDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author test
 */
@WebServlet(urlPatterns = {"/loginAdminjsp"})
public class loginAdminjsp extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet loginAdminjsp</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet loginAdminjsp at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String userAdmin = request.getParameter("userAdmin");
        String passAdmin = request.getParameter("passAdmin");
        // check condition from login
        LoginDao dao = new LoginDao();
        if(dao.checkLogin(userAdmin, passAdmin))
        {
            HttpSession session = request.getSession();
            session.setAttribute("userAdmin", userAdmin);
            response.sendRedirect("indexAdmin.jsp");
        }
        else
        {
            response.sendRedirect("index.jsp");
            
        }
        processRequest(request, response);
        processRequest(request, response);
    }

    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

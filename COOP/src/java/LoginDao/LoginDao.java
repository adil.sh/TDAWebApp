package LoginDao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author test
 */
public class LoginDao {
    String URL=  "jdbc:mysql://localhost:3306/tdadb"; 
    String Username = "root";
    String Password = "";
    
    String SQLQuery = "Select * from admin where username =? and password=?";
    public boolean checkLogin(String username, String password)
    {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection(URL,Username,Password);
            PreparedStatement st = con.prepareStatement(SQLQuery);
            st.setString(1, username);
            st.setString(2, password);
            ResultSet rs = st.executeQuery();
            
            if(rs.next())
            {
                return true;
            }
            
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(LoginDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(LoginDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return false;
    }
}
